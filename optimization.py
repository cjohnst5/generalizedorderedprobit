# Authors: Carla Johnston and Kramer Quist
# optimize_func written mostly by carla
# fix_solution and its various subfunctions written by Kramer.
# Last updated August 8, 2018 by Carla.

from __future__ import division
from sgtcdfs import *
from loglike import llv
from scipy.optimize import minimize


def calling_opt(cdf_case, het_form, Y, X, start_values, method='Nelder-Mead', options=({'maxiter': 20000, 'maxfev': 20000})):
    """
    Calls the optimize_func to obtain betas, alphas, thetas and deltas
    Also returns the start values, loglikelihood values, the pdf and cdf that we pick
    up on the way.
    """
    est_param = optimize_func(cdf_case, het_form, Y, X, start_values, method, options)
    return est_param


def optimize_func(cdf_case, het_form, Y, X, start_values, method_opt='Nelder-Mead', options=({'maxiter': 20000, 'maxfev' : 20000}),
    fix_v=1.,fix_g=1.,fix_c=1.):
    
    """
    Main optimization function. 

    Parameters
    -----------------------------------
    cdf_case: 
        1: Normal
        2: Laplace
        3: Snormal
        4: GED
        5: Slaplace
        6: SGED
        7: ST, but this is still a work in progress
    het_form:
        If homoskedasticity is assumed, then het_form should be None. 
        If heteroskedasticity is assumed: 
        Numpy array or vector which holds the x's that are suspected to cause
        heteroskedasticity.
    Y:
        numpy vector of categorical outcome variable. Categories must start at 0 and the vector needs to be of int type.
    X:
        numpy array or vector. Needs to have the same number of rows as Y.
    start_values:
        our initial guess for the alphas, betas, thetas and in the case of heteroskedasticity, deltas. Returned from
        init_guess_calc in the initial_values file
    method_opt:
        String. Optimization method to use for the scipy.optimize minimize function. Acceptable methods can be found here:
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html
    options:
        Dictionary of scipy.optimize.minimize options such as maximum number of iterations and tolerance levels
    fix_c:
        TODO
    fix_g:
        TODO
    fix_v:
        TODO
    Returns
    ----------------------------------
    The resultant object from the optimization procedure. This object contains unknown alphas (cutoffs) and
    betas (coefficients)  and thetas (distributional parameters), along with heteroskedasticity
    effects if heteroskedasticity is specified. 

    """ 
    k = len(set(Y))
    numabs = k - 1 + X.shape[1]
    if het_form != None:
        numdelts = het_form.shape[1]  

    if cdf_case <= 2 or cdf_case == 8: #normal, logit and laplace don't have distributional parameters
                        #so we have to set up their criteria a little bit differently. 
        if het_form == None:             
            loglike_obj = lambda x: llv(x, [0], X, Y, cdf_case, k, het_form, deltas = None)
        else:
            loglike_obj = lambda x: llv(x[0:numabs], [0], X, Y, cdf_case, k, het_form, x[-numdelts:])
    else: #we can treat the rest of these distributions the same way 
        if het_form == None:                        
            loglike_obj = lambda x: llv(x[0:numabs], x[numabs:], X, Y, cdf_case, k, het_form, deltas = None)
            # loglike_obj = lambda x: llv(x[0:numabs], [2], X, Y, cdf_case, k, het_form, deltas = None)

        else:
            loglike_obj = lambda x: llv(x[0:numabs], x[numabs:-numdelts], X, Y, cdf_case, k, het_form, x[-numdelts:])

    est_param = minimize(loglike_obj, start_values, method = method_opt, options = options)

    if est_param.success == 0:
        options = ({'maxfev': 100000})
        est_param = minimize(loglike_obj, est_param.x, method='Powell', options=options)
    if cdf_case == 8:
        num_abs = k-1+X.shape[1]
        est_param.x[:num_abs] = standardize_logit(est_param.x[:num_abs])
    return est_param

# ==========================
# Subfunctions
# ==========================

def standardize_logit(alphas_betas):
     return alphas_betas*math.sqrt(3)/math.pi






# written by Carla
# Last updated August 6, 2018 by Carla

from __future__ import division
import pandas as pd
from helper_functions import unpack_parameters
import os.path




def write_to_excel(parameters, errors, num_alphas, num_betas, het_form, ll_restricted, lr_test, cdf_case, obs):
    """

    :param parameters:
        The resultant object from from optimize_func() in optimize.py
    :param errors:
        Numpy vector of errors returned from std_errs() in std_errs.py
    :param num_alphas:
        Scalar. Number of cutoffs.
    :param num_betas
        Scalar. Number of coefficients
    :param het_form
        Intended to allow for heteroskedastic standard errors, currently not functional. Set to None
    :param ll_restricted:
        Scalar. Restricted loglikelihood function value as calculated in restricted_calc() loglike.py
    :param lr_test:
        Scalar. Result of the loglikelihood test calculated in lr_test() in loglike.py
    :param cdf_case:
        Scalar
        1: Normal
        2: Laplace
        3: Snormal
        4: GED
        5: Slaplace
        6: SGED
        7: ST, but this is still a work in progress
    :param obs:
        Number of observations used in data.
    :return:
        Outputs csv file with observation results
    """

    # Parameters and estimates
    alphas, betas, thetas, deltas = unpack_parameters(parameters, het_form, num_alphas, num_betas)
    alphas_steds = errors[0:num_alphas]
    betas_steds = errors[num_alphas:]

    # Optimization success and loglikehood
    parameters_success = [parameters.success]
    ll_restricted = [ll_restricted]
    ll_unrestricted = [-1 * parameters.fun]
    lr_test = [lr_test]
    i = ['success', 'alphas', 'alphas_steds', 'betas', 'betas_steds', 'thetas', 'll_restricted', 'll_unrestricted', 'lr_test']

    df_a = pd.DataFrame(data=alphas).transpose()
    df_b = pd.DataFrame(data=alphas_steds).transpose()
    df_t = pd.DataFrame(data=betas).transpose()
    df_bs = pd.DataFrame(data=betas_steds).transpose()
    df_as = pd.DataFrame(data=thetas).transpose()
    df_lr = pd.DataFrame(data=ll_restricted).transpose()
    df_ur = pd.DataFrame(data=ll_unrestricted).transpose()
    df_lt = pd.DataFrame(data=lr_test).transpose()
    df_s = pd.DataFrame(data=parameters_success).transpose()

    df_a = df_s.append(df_a)
    df_a = df_a.append(df_b)
    df_a = df_a.append(df_t)
    df_a = df_a.append(df_bs)
    df_a = df_a.append(df_as)
    df_a = df_a.append(df_lr)
    df_a = df_a.append(df_ur)
    df_a = df_a.append(df_lt)

    df_a = pd.DataFrame(data=df_a.values,index=i)

    if cdf_case == 1:
        dist = 'normal'
    elif cdf_case == 2:
        dist = 'laplace'
    elif cdf_case == 3:
        dist = 'snormal'
    elif cdf_case == 4:
        dist = 'ged'
    elif cdf_case == 5:
        dist = 'slaplace'
    elif cdf_case == 6:
        dist = 'sged'
    elif cdf_case == 7:
        dist = 'st'
    elif cdf_case == 8:
        dist = 'logit'

    filename = dist + '_with_' + str(obs)+'_obs.csv'
    here = os.path.dirname(os.path.realpath(__file__))
    subdir = 'output_files'
    filepath = os.path.join(here, subdir, filename)
    df_a.to_csv(filepath)


def print_output(cdf_case, parameters, errors, het_form, num_alphas, num_betas, ll_restricted, lr_test, obs):

    """
    Takes the results of calling opt and organizes them in an easy to read fashion
    Also calls the restricted loglikelihood function.
    """
    if cdf_case == 1:
        dist = 'normal'
    elif cdf_case == 2:
        dist = 'laplace'
    elif cdf_case == 3:
        dist = 'snormal'
    elif cdf_case == 4:
        dist = 'ged'
    elif cdf_case == 5:
        dist = 'slaplace'
    elif cdf_case == 6:
        dist = 'sged'
    elif cdf_case == 7:
        dist = 'st'
    elif cdf_case == 8:
        dist = 'logit'



    if parameters.success == 1:
        ll_unrestricted = -1 * parameters.fun

        alphas, betas, thetas, deltas = unpack_parameters(parameters, het_form, num_alphas, num_betas)
        numalphas = alphas.size

        alphas_steds = errors[0:numalphas]
        betas_steds = errors[numalphas:]

        print '********** ' + dist + ' DISTRIBUTION***************************'
        print "Estimated Betas and Standard errors"
        print ["Betas", list(betas)]
        print ["Std Errs", list(betas_steds)], '\n'

        print "Estimated Cutoffs"
        print ["Alphas", list(alphas)]
        print ["Std Errs", list(alphas_steds)], '\n'

        if cdf_case > 2 and cdf_case != 8:
            print "Distributional Parameters"
            print thetas, '\n'

        print ["Restricted Log-Likelihood:", ll_restricted], '\n'
        print ["Unrestricted Log-Likelihood", ll_unrestricted], '\n'
        print ["LR Test: Restricted vs. Unrestricted", lr_test], '\n'

    elif parameters.success == 0:
        print "Oh dear. Try something new, like changing the number of iterations or the estimation method"
    else:
        print "Something else is wrong"

# Written by Carla
# Last updated August 6, 2018 by Carla.

import sgtcdfs as sgtc
import sgtpdfs as sgtp
import numpy as np

def unpack_parameters(parameters, het_form, numalphas, numbetas):
    """

    :param parameters:  The resultant object from from optimize_func() in optimize.py
    :param het_form: If homoskedasticity is assumed, then het_form should be None.
        If heteroskedasticity is assumed:
        Numpy array or vector which holds the x's that are suspected to cause
        heteroskedasticity.
    :param numalphas: number of cutoffs
    :param numbetas: number of coefficients
    :return: cutoffs, coefficients, distributional parameters and in the case of heteroskedasticity, the deltas.
    """
    alphas = parameters.x[0:numalphas]
    betas = parameters.x[numalphas:(numalphas + numbetas)]

    deltas = []
    if het_form != None:
        numhetx = het_form.shape[1]
        thetas = parameters.x[(numalphas + numbetas):-numhetx]
        deltas = parameters.x[-numhetx:]
    elif het_form == None:
        thetas = parameters.x[(numalphas + numbetas):]
        deltas = None

    return alphas, betas, thetas, deltas


def choose_pdfcdf(cdf_case):
    """
    Returns the appropriate cdf and pdf functions given the chosen distribution

    """
    if cdf_case == 1:
        cdf = sgtc.normal_cdf
        pdf = sgtp.normal_pdf
    elif cdf_case == 2:
        cdf = sgtc.laplace_cdf
        pdf = sgtp.laplace_pdf
    elif cdf_case == 3:
        cdf = sgtc.snormal_cdf
        pdf = sgtp.snormal_pdf
    elif cdf_case == 4:
        cdf = sgtc.ged_cdf
        pdf = sgtp.ged_pdf
    elif cdf_case == 5:
        cdf = sgtc.slaplace_cdf
        pdf = sgtp.slaplace_pdf
    elif cdf_case == 6:
        cdf = sgtc.sged_cdf
        pdf = sgtp.sged_pdf
    elif cdf_case == 7:
        cdf = sgtc.st_cdf
        pdf = sgtp.st_pdf
    elif cdf_case == 8:
        cdf = sgtc.logit_cdf
        pdf = sgtp.logit_pdf
    return pdf, cdf



def sigma_calc(het_form, obs, deltas=None):
    """
    Returns either a column of ones or the heteroskedasticity for each observation,
    given the provided x-variables.

    Because heteroskedasticity is not a working feature, this will simply retrun column of ones.

    Parameters:
    ------------------------------------------------------------------
    het_form:
        numpy array or vector of independent variables that are assumed to
        affect variance or None if homoskedasticity is assumed.
    obs:
        number of observations. Scalar
    deltas:
        Numpy vector or python list. effects of x on heteroskedasticity
    """

    if het_form == None:
        sig = np.ones(obs)
    else:
        sig = np.exp(np.dot(het_form, deltas))
    return sig

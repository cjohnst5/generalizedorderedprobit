from __future__ import division
import numpy as np
import optimization as om
import scipy.linalg as la


def int_guess_calc(Y, X, cdf_case, het_form=None,):
    """
    Comes up with initial guesses for alphas, betas, thetas and deltas (if there is hetero) to put into our
    optimization function.
    Alphas are the cutoffs for our categorical variable, betas are the slope coefficients, thetas
    are the distributional parameters if there is heteroskedasticity, deltas are the heteroskedastic
    x's effects on variance.

    Parameters:
    ----------------------------------
    Y: numpy vector of categorical variable (must start at 0)
    X: numpy array or vector
    cdf_case:
        1: Normal
        2: Laplace
        3: Snormal
        4: GED
        5: Slaplace
        6: SGED
        7: SGT
        8: Logit
    het_form:
        Either None or the X array which contains the X variables suspected of causing hetero
    Returns:
    ----------------------------------
    start_values:
        our initial guess for the alphas, betas, thetas and deltas(if heteroskedasticity is assumed)
    ll_homo:
        If heteroskedasticity is incorporated, this is the loglikelihood value
        from estimating the model using the specified distribution, assuming homoskedasticity.
    """
    unique_cat = set(Y)
    numcat = len(unique_cat)
    n = len(Y)
    startbetas = la.lstsq(X, Y)[0]

    # OLS to start us off, binary outcome variable
    if numcat == 2:
        y_pred = np.dot(X, startbetas)
        max_min = y_pred.max() - y_pred.min()
        alphas = [max_min * (1 / numcat) + y_pred.min()]
        startalphas = np.asarray(alphas)
        sample_std_dev = (la.lstsq(X, Y)[1] / len(X)) ** .5

    # More than 2 categories for outcome variable. Taken from Kramer's par_ordered_main.py
    if numcat > 2:
        for j in range(5):
            alphas = []
            y_pred = np.dot(X, startbetas)
            max_min = y_pred.max() - y_pred.min()
            for i in range(1, numcat):
                alphas.append(max_min * (i / numcat) + y_pred.min())
            startalphas = np.asarray(alphas)
            alpha_mids = np.zeros(len(startalphas) + 1)
            for i in range(1, len(alpha_mids) - 1):
                alpha_mids[i] = .5 * startalphas[i] + .5 * startalphas[i - 1]
            alpha_mids[0] = 2 * startalphas[0] - startalphas[1]
            alpha_mids[-1] = 2 * startalphas[-1] - startalphas[-2]
            startbetas = la.lstsq(X, alpha_mids[Y])[0]

        sample_std_dev = (la.lstsq(X, alpha_mids[Y])[1] / len(X)) ** .5

    # Getting CDF specific start values
    start_ab = np.zeros(len(startalphas) + len(startbetas))
    start_ab[0:len(startalphas)] = startalphas
    start_ab[len(startalphas):] = startbetas
    start_ab /= sample_std_dev #not sure why we are doing this.
    # numab = len(start_ab)
    if het_form != None:
        numdelts = het_form.shape[1]
        start_delts = np.zeros(numdelts)

    if cdf_case == 1 or cdf_case == 8:
        if het_form == None:
            start_values = start_ab #since we adjust the variance incide the cdf now, we don't have to adjust our starting values
        elif het_form != None:
            results = om.optimize_func(1, None, Y, X, start_ab)
            start_ab, ll_func = results.x, -results.fun
            start_values = np.concatenate((start_ab, start_delts))

    elif cdf_case == 2:
        start_values = start_ab
        if het_form != None:
            results = om.optimize_func(2, None, Y, X, start_values)
            start_ab, ll_func = results.x, -results.fun
            start_values = np.concatenate((start_ab, start_delts))

    elif cdf_case == 3:
        start_ab = om.optimize_func(1, None, Y, X, start_ab).x
        start_values = np.concatenate((start_ab, [0]))
        if het_form != None:
            results = om.optimize_func(3, None, Y, X, start_values)
            start_ab, ll_func = results.x, -results.fun
            start_values = np.concatenate((start_ab, start_delts))

    elif cdf_case == 4:
        start_ab = om.optimize_func(1, None, Y, X, start_ab).x
        start_values = np.concatenate((start_ab, [2]))
        if het_form != None:
            results = om.optimize_func(4, None, Y, X, start_values)
            start_ab, ll_func = results.x,-results.fun
            start_values = np.concatenate((start_ab, start_delts))

    elif cdf_case == 5:
        start_ab = om.optimize_func(1, None, Y, X, start_ab).x
        start_values = np.concatenate((start_ab, [0]))
        if het_form != None:
            results = om.optimize_func(5, None, Y, X, start_values)
            start_ab, ll_func = results.x, -results.fun
            start_values = np.concatenate((start_ab, start_delts))

    elif cdf_case == 6:
        start_ab = om.optimize_func(1, None, Y, X, start_ab).x
        start_values_slaplace = np.concatenate((start_ab, [0]))
        start_values = om.optimize_func(5, None, Y, X, start_values_slaplace).x
        start_values = np.concatenate((start_values, [1]))
        if het_form != None:
            results = om.optimize_func(6, None, Y, X, start_values)
            start_ab, ll_func = results.x, -results.fun
            start_values = np.concatenate((start_ab, start_delts))

    elif cdf_case == 7:
        start_ab = om.optimize_func(1, None, Y, X, start_ab).x
        start_values = np.concatenate((start_ab, [0, 100]))
        if het_form != None:
            results = om.optimize_func(7, None, Y, X, start_values)
            start_ab, ll_func = results.x, -results.fun
            start_values = np.concatenate((start_ab, start_delts))
    if het_form == None:
        ll_func = []

    return start_values, ll_func

# def int_guess_calc(Y, X, cdf_case, het_form=None):
#     """
#     Comes up with initial guesses for alphas, betas, thetas and deltas (if there is hetero) to put into our
#     optimization function.
#     Alphas are the cutoffs for our categorical variable, betas are the slope coefficients, thetas
#     are the distributional parameters if there is heteroskedasticity, deltas are the heteroskedastic
#     x's effects on variance.
#
#     Parameters:
#     ----------------------------------
#     Y: numpy vector of categorical variable (must start at 0)
#     X: numpy array or vector
#     cdf_case:
#         1: Normal
#         2: Laplace
#         3: Snormal
#         4: GED
#         5: Slaplace
#         6: SGED
#         7: ST. This is still a work in progress
#     het_form:
#         Either None or the X array which contains the X variables suspected of causing hetero
#     Returns:
#     ----------------------------------
#     start_values:
#         our initial guess for the alphas, betas, thetas and in the case of heteroskedasticity, deltas
#     ll_homo:
#         If heteroskedasticity is incorporated, this is the loglikelihood value
#         from estimating the model using the specified distribution, assuming homoskedasticity.
#     """
#
#     startbetas = la.lstsq(X, Y)[0]
#     alphas = []
#     # getting alpha estimates using predicted y's from OLS.
#     unique_cat = set(Y)
#     numcat = len(unique_cat)
#     y_pred = np.dot(X, startbetas)
#     max_min = y_pred.max() - y_pred.min()
#
#     for i in xrange(1, numcat):
#        alphas.append(max_min * (i/numcat) + y_pred.min())
#     startalphas = np.asarray(alphas)
#
#     # using some OLS and divying to start us off
#     start_ab = np.zeros(len(startalphas) + len(startbetas))
#     start_ab[0:len(startalphas)] = startalphas
#     start_ab[len(startalphas):] = startbetas
#
#     if het_form != None:
#         numdelts = het_form.shape[1]
#         start_delts = np.zeros(numdelts)
#
#     if cdf_case == 1 or cdf_case == 8:
#         if het_form == None:
#             start_values = start_ab
#         elif het_form != None:
#             results = om.optimize_func(1, sgtc.normal_cdf, None, Y, X, start_ab)
#             start_ab, ll_func = results.x, -results.fun
#             start_values = np.concatenate((start_ab, start_delts))
#
#     elif cdf_case == 2:
#         start_values = om.optimize_func(1, sgtc.normal_cdf, None, Y, X, start_ab).x
#         if het_form != None:
#             results = om.optimize_func(2, sgtc.laplace_cdf, None, Y, X, start_values)
#             start_ab, ll_func = results.x, -results.fun
#             start_values = np.concatenate((start_ab, start_delts))
#
#     elif cdf_case == 3:
#         start_ab = om.optimize_func(1, sgtc.normal_cdf, None, Y, X, start_ab).x
#         start_values = np.concatenate((start_ab, [0]))
#         if het_form != None:
#             results = om.optimize_func(3, sgtc.snormal_cdf, None, Y, X, start_values)
#             start_ab, ll_func = results.x, -results.fun
#             start_values = np.concatenate((start_ab, start_delts))
#
#     elif cdf_case == 4:
#         start_ab = om.optimize_func(1, sgtc.normal_cdf, None, Y, X, start_ab).x
#         start_values = np.concatenate((start_ab, [2]))
#         if het_form != None:
#             results = om.optimize_func(4, sgtc.ged_cdf, None, Y, X, start_values)
#             start_ab, ll_func = results.x,-results.fun
#             start_values = np.concatenate((start_ab, start_delts))
#
#     elif cdf_case == 5:
#         start_ab = om.optimize_func(1, sgtc.laplace_cdf, None, Y, X, start_ab).x
#         start_values = np.concatenate((start_ab, [0]))
#         if het_form != None:
#             results = om.optimize_func(5, sgtc.slaplace_cdf, None, Y, X, start_values)
#             start_ab, ll_func = results.x, -results.fun
#             start_values = np.concatenate((start_ab, start_delts))
#
#     elif cdf_case == 6:
#         start_ab = om.optimize_func(1, sgtc.laplace_cdf, None, Y, X, start_ab).x
#         start_values_slaplace = np.concatenate((start_ab, [0]))
#         start_values = om.optimize_func(5, sgtc.slaplace_cdf, None, Y, X, start_values_slaplace).x
#         start_values = np.concatenate((start_values, [1]))
#         if het_form != None:
#             results = om.optimize_func(6, sgtc.sged_cdf, None, Y, X, start_values)
#             start_ab, ll_func = results.x, -results.fun
#             start_values = np.concatenate((start_ab, start_delts))
#
#     elif cdf_case == 7:
#         start_ab = om.optimize_func(1, sgtc.normal_cdf, None, Y, X, start_ab).x
#         start_values = np.concatenate((start_ab, [0, 100]))
#         if het_form != None:
#             results = om.optimize_func(7, sgtc.st_cdf, None, Y, X, start_values)
#             start_ab, ll_func = results.x, -results.fun
#             start_values = np.concatenate((start_ab, start_delts))
#
#
#     if het_form == None:
#         ll_func = []
#
#     return start_values, ll_func









        




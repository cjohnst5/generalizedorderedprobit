# Written by Kramer
# Last updated August 6, 2018 by Carla

from __future__ import division
import numpy as np
from scipy.special import  beta, gamma, gammainc, betainc, erf

import math
"""
cdfs of the SGT trees. Includes SGED, Snormal, GED, Slaplace, Laplace, Normal cdfs
Also logit cdf

FOR THE SGT FAMILY
-Assuming the mean = 0 on all distributions for identification purposes.
-Here we set sigma (not variance)=1 for all distributions by dividing by standard deviations of the appropriate distributions.
This will result in the estimation of standardized alphas and betas in optimization.py

FOR LOGIT
No sigma standardization occurs within its cdf function. Alphas and betas are standardized in the optimize function.

paravec: List of arrays. The first array (paravec[0]) is the sigma array. It is of size n, where n is the number of observations.
    The purpose of the sigma numpy array is to allow for heteroskedasticity, but heteroskedasticity is not currently functional
    so paravec[0] is always a list of ones, as calculated by sigma_calc() in helper_functions.py
    paravec[1] is a list containing the other distributional parameters if the distribution requires them.
y: numpy vector of categorical variable (must start at 0)
"""

def normal_cdf(paravec, y):
    sig = paravec[0]
    phi= sig / std_dev_normal()
    z=(y/phi)**2
    cdf=.5*(1+np.sign(y)*gammainc(1/2,z))
    return cdf

def ged_cdf(paravec, y):
    sig = paravec[0]
    phi= sig / std_dev_ged(paravec[1])
    p=paravec[1][0]
    z=abs(y/phi)**p
    cdf = .5*(1+np.sign(y)*gammainc(1/p, z)) 
    return cdf

def snormal_cdf(paravec, y):
    sig = paravec[0]
    phi= sig / std_dev_snormal(paravec[1])
    lamb=paravec[1][0]
    m=-2*lamb*phi*(gamma(1)/gamma(.5))
    z=(y-m)**2/(phi**2*(1+lamb*np.sign(y-m))**2)
    cdf=(1-lamb)/2+((1+lamb*np.sign(y-m))/2)*np.sign(y-m)*gammainc(1/2,z)
    return cdf

def laplace_cdf(paravec,y):
    sig = paravec[0]
    phi = sig / std_dev_laplace()
    cdf = .5*(1+np.sign(y)*(1-np.exp(-abs(y)/phi)))
    return cdf 

def slaplace_cdf(paravec, y):
    sig = paravec[0]
    phi = sig / std_dev_slaplace(paravec[1])
    lamb = paravec[1][0]
    m=-2*lamb*phi
    z=abs(y-m)/(phi*(1+lamb*np.sign(y-m)))
    cdf=(1-lamb)/2+((1+lamb*np.sign(y-m))/2)*np.sign(y-m)*gammainc(1,z)
    return cdf

def sged_cdf(paravec, y):
    sig = paravec[0]
    phi = sig / std_dev_sged(paravec[1])
    lamb, p = paravec[1]
    m=-2*lamb*phi*gamma(2/p)/gamma(1/p)
    z =abs(y-m)**p/ (phi ** p * (1 + lamb * np.sign(y-m)) ** p)
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1/p, z)
    return cdf

def st_cdf(paravec, y):
    sig = paravec[0]
    phi = sig / std_dev_st(paravec[1])
    lamb, q = paravec[1]
    m = -2 * lamb * phi * (math.sqrt(q) * beta(1, q - .5) / beta(.5, q))
    z = (y - m) ** 2 / ((y - m) ** 2 + q * phi ** 2 * (1 + lamb * np.sign(y - m)) ** 2)
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y - m)) / 2) * np.sign(y - m) * betainc(.5, q, z)
    return cdf

def logit_cdf(paravec,y):
    sig = paravec[0]
    cdf = 1/(1+np.e**-y/sig)
    return cdf

def std_dev_st(thetas):

    lamb,q=thetas
    m=-2*lamb*(math.sqrt(q)*beta(1,q-.5)/beta(.5,q))
    adj_var=q*beta(1.5,q-1)/beta(.5,q)*(3*lamb**2+1)-m**2
    #print adj_var
    return math.sqrt(adj_var)


    #     lamb, p, q=thetas
    #     adj_var=q**(2/p)*beta(3/p,q-2/p)/beta(1/p,q)*(3*lamb**2+1)
    #     return math.sqrt(adj_var)
    # return std_dev_sged(thetas)
    """
    FIX ME TO ST BEFORE FINAL
    lamb,q=thetas
    return std_dev_sgt([lamb,2.,q])
    """
def std_dev_sged(thetas):
    lamb,p=thetas
    m=-2*lamb*gamma(2/p)/gamma(1/p)
    adj_var=gamma(3/p)/gamma(1/p)*(3*lamb**2+1)-m**2
    return math.sqrt(adj_var)

def std_dev_ged(thetas):
    p=thetas[0]
    adj_var=gamma(3/p)/gamma(1/p)
    return math.sqrt(adj_var)
def std_dev_slaplace(thetas):
    lamb=thetas[0]
    m=-2*lamb
    adj_var=(6*lamb**2+2)-m**2
    return math.sqrt(adj_var)
def std_dev_snormal(thetas):
    lamb=thetas[0]
    m=-2*lamb*.56149
    adj_var=.5*(3*lamb**2+1)-m**2
    return math.sqrt(adj_var)
def std_dev_laplace():
    #adj_var=phi**2*gamma(3)
    return math.sqrt(2)
def std_dev_normal():
    return math.sqrt(.5)
def std_dev_logit():
    return math.pi/math.sqrt(3)

# -------------SGT. Not functional, but here's a start on the cdf and related functions --------------------------
# def sgt_cdf(paravec, y):
#     phi = paravec[0]
#     lamb,p,q = paravec[1]
#     z = abs(y) ** p / (abs(y)**p + q * phi**p * (1 + lamb * np.sign(y))**p)
#     cdf = (1-lamb)/ 2 + ((1 + lamb * np.sign(y)) / 2) * np.sign(y) * betainc(1/p, q, z)
#     return cdf
#
# def phi_sgt(sig, lamb, q, p):
#     phi = (sig / q**(1/p)) * (beta(1/p, q) / (beta(3/p, q-2/p) + lamb ** 2\
#         *(3 * beta(3/p, q-2/p) - 4 * beta(2/p, q-1/p)**2 / beta(1/p, q)))) **(1/2)
#     return phi
#
# def std_dev_sgt(thetas):
#     lamb, p, q=thetas
#     adj_var=q**(2/p)*beta(3/p,q-2/p)/beta(1/p,q)*(3*lamb**2+1)
#     return math.sqrt(adj_var)
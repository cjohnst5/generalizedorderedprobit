# Written by Carla
# Last updated August 6, 2018 by Carla

from __future__ import division
import numpy as np
from scipy.special import betaln, beta, gamma
from sgtcdfs import *

"""
PDFS for the SGT tree. Also include logit.

FOR THE SGT FAMILY
-Assuming the mean (mu) = 0 on all distributions.
-Here we set sigma (not variance)=1 for all distributions by dividing by standard deviations of the appropriate distributions.
This will result in the estimation of standardized alphas and betas in optimization.py


FOR LOGIT
-It's sigma is standardized by dividing by the standard deviation of the logit distribution.
within the pdf.

paravec: List of lists. The first list (paravec[0]) is the sigma list. It is of size n, where n is the number of observations.
    The purpose of the sigma list is to allow for heteroskedasticity, but heteroskedasticity is not currently functional
    so paravec[0] is always a list of ones, as calculated by sigma_calc() in helper_functions.py
    paravec[1] is a list containing the other distributional parameters.
y: numpy vector of categorical variable (must start at 0)

"""


def st_pdf(paravec, y):
    sig = paravec[0]
    lamb, q = paravec[1][0:]
    p = 2

    phi = (sig / q**(1/p)) * (beta(1/p, q) / (beta(3/p, q-2/p) + lamb ** 2\
        *(3 * beta(3/p, q-2/p) - 4 * beta(2/p, q-1/p)**2 / beta(1/p, q)))) **(1/2)
    m = -2 * lamb * phi * (math.sqrt(q) * beta(1, q - .5) / beta(.5, q))

    pdf = np.exp((np.log(p)-np.log(2)-np.log(phi)-1/p*np.log(q)-betaln(q,1/p))+(-(q+1/p)\
                        *np.log(1+abs(y-m)**p/q/(phi**p)/(1+lamb*np.sign(y-m))**p)));
    return pdf

def sged_pdf(paravec, y):
    sig = paravec[0]

    lamb, p = paravec[1]
    phi = sig * (gamma(1/p) / (gamma(3/p) + lamb ** 2 * \
         (3 * gamma(3/p) - 4 * gamma(2/p)**2 / gamma(1/p))))**(1/2)
    m=-2*lamb*phi*gamma(2/p)/gamma(1/p)

    pdf = p * np.exp(- abs(y-m) ** p / (( 1 + lamb * np.sign(y-m)) * phi) ** p) \
          / (2 * phi * gamma(1/p))
    return pdf

def slaplace_pdf(paravec, y):    
    sig = paravec[0]
    lamb = paravec[1][0]

    phi = sig * (gamma(1) / (gamma(3) + lamb ** 2 * \
         (3 * gamma(3) - 4 * gamma(2)**2 / gamma(1))))**(1/2)
    m=-2*lamb*phi
    pdf = np.exp(- abs(y-m) / (( 1 + lamb * np.sign(y-m)) * phi)) \
          / (2 * phi * gamma(1))
    return pdf

def ged_pdf(paravec, y):
    m = 0
    lamb = 0
    sig = paravec[0]
    p = paravec[1][0]
    phi = sig * (gamma(1/p) / (gamma(3/p) + lamb ** 2 * \
         (3 * gamma(3/p) - 4 * gamma(2/p)**2 / gamma(1/p))))**(1/2)

    pdf = p * np.exp(-abs(y-m) ** p / phi ** p ) \
          / (2 * phi * gamma(1/p))
    return pdf

def snormal_pdf(paravec, y):
    p = 2
    sig = paravec[0]
    lamb = paravec[1][0]

    m=-2*lamb*sig*(gamma(1)/gamma(.5))

    phi = sig * (gamma(1/p) / (gamma(3/p) + lamb ** 2 * \
         (3 * gamma(3/p) - 4 * gamma(2/p)**2 / gamma(1/p))))**(1/2) 

    pdf = np.exp(- abs(y-m) ** 2 / (( 1 + lamb * np.sign(y-m)) * phi) ** 2) \
          / (phi * gamma(1/2))
    return pdf

def normal_pdf(paravec, y):
    m = 0
    sig = paravec[0]
    phi = sig / std_dev_normal()
    pdf = np.exp(- abs(y-m) ** 2 /  phi ** 2) \
          / (phi * gamma(1/2))

    return pdf

def laplace_pdf(paravec, y):
    m, lamb, p = 0, 0, 1
    sig = paravec[0]
    phi = sig * (gamma(1/p) / (gamma(3/p) + lamb ** 2 * \
         (3 * gamma(3/p) - 4 * gamma(2/p)**2 / gamma(1/p))))**(1/2)
    pdf = p * np.exp(- abs(y-m) ** p / (( 1 + lamb * np.sign(y-m)) * phi) ** p) \
          / (2 * phi * gamma(1/p))
    return pdf

def logit_pdf(paravec, y):
    phi = paravec[0]/std_dev_logit()
    pdf=(np.e**-(y/phi))/(phi*(np.e**(-2*(y/phi))+2*np.e**-(y/phi)+1))
    return pdf

# -------------SGT. Not functional, but here's the pdf --------------------------
# def sgt_pdf(paravec,y):
#     sig = paravec[0] / std
#     lamb,p,q = paravec[1][0:]
#
#     phi = (sig / q**(1/p)) * (beta(1/p, q) / (beta(3/p, q-2/p) + lamb ** 2\
#         *(3 * beta(3/p, q-2/p) - 4 * beta(2/p, q-1/p)**2 / beta(1/p, q)))) **(1/2)
#     m = 2*phi*lamb*q**(1/p)*beta(2/p, q-1/p)/beta(1/p,q)
#
#     pdf = np.exp((np.log(p)-np.log(2)-np.log(phi)-1/p*np.log(q)-betaln(q,1/p))+(-(q+1/p)\
#                         *np.log(1+abs(y-m)**p/q/(phi**p)/(1+lamb*np.sign(y-m))**p)));
#     return pdf
# Written by Carla
# Last updated August 6, 2018 by Carla
import numpy as np
import initial_values as iv
import optimization as op
from helper_functions import unpack_parameters
import std_errs as se
import loglike as lg
import output as ot
import sys
import time
import os.path

'''

'''

# **********************************************************************************************************************
# Reading in data
# **********************************************************************************************************************
'''
data: Needs to be in a .csv file with first column containing outcome variable. The program assumes first row contains
    column names
start_index: If you want to only read in a certain section of observations, specify the start row index
obs: Number of observations you want to use from your dataset
'''
data = np.loadtxt('Data\worldvalues10outcomes1000obsreligiousmarried.csv', delimiter=',', skiprows = 1)

start_index = 0
obs = 1000
Y = data[start_index:start_index+obs, 0]
Y = Y.astype(int)
X = data[start_index:start_index + obs, 1:]
numalphas = len(set(Y)) - 1
numbetas = X.shape[1]


# **********************************************************************************************************************
# Initial parameters
# **********************************************************************************************************************

'''
cdf_case:
        1: Normal
        2: Laplace
        3: Snormal
        4: GED
        5: Slaplace
        6: SGED
        7: ST. This is still pretty slow on optimization
        8: Logit

het_form: Intended to allow for heteroskedastic standard errors, currently not functional
       Set to None

method_std: Choose which estimator to use for standard errors
        optgradient: Uses the inverse of the gradient
        invneghess:  Uses the inverse of the Hessian
        sandwich: Sandwich estimator
'''
# Distribution, heteroskedasticity, standard errors
cdf_case = 7
het_form = None
method_std = 'optgradient'

# Optimization parameters
'''
method_opt: Optimization method to use for the scipy.optimize minimize function. Acceptable methods can be found here:
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html

options: Dictionary of scipy.optimize.minimize options such as maximum number of iterations and tolerance levels
'''
method_opt = 'Nelder-Mead'
options = ({'maxiter': 20000, 'maxfev': 20000})
options = ({'maxfev': 100000, 'maxiter' : 100000, 'xtol' : 1e-8})
options = ({'maxiter': 100000, 'maxfev' : 100000})

# **********************************************************************************************************************
# Running the program
# **********************************************************************************************************************
# Setting up the printed output file
timestr = time.strftime("%Y%m%d_%H%M%S")
filename = timestr + '_with_' + str(obs) + '_obs.out'
here = os.path.dirname(os.path.realpath(__file__))
subdir = 'output_files'
filepath = os.path.join(here, subdir, filename)
f = open(filepath, 'w')
sys.stdout = f

# for cdf_case in range(1, 4):

# Obtaining starting values
start_values, ll_homo = iv.int_guess_calc(Y, X, cdf_case, het_form)

# Obtaining and unpacking estimated parameters
parameters = op.calling_opt(cdf_case, het_form, Y, X, start_values, method_opt, options)

alphas, betas, thetas, deltas = unpack_parameters(parameters, het_form, numalphas, numbetas)

# Obtaining standard errors
errors = se.std_errs(Y, X, alphas, betas, thetas, het_form, cdf_case, deltas, method_std)

# Obtaining loglikelihood test
ll_result = lg.lr_test(Y, X, method_opt, options, parameters)
ll_restricted = lg.restricted_calc(Y, X, method_opt, options)

# Printing output
ot.print_output(cdf_case, parameters, errors, het_form, numalphas, numbetas, ll_restricted, ll_result, obs)

# Exporting output to Excel file
ot.write_to_excel(parameters, errors, numalphas, numbetas, het_form, ll_restricted, ll_result, cdf_case, obs)
f.close()


# written by Carla
# Last updated August 6, 2018 by Carla
from __future__ import division
import numpy as np
import scipy.linalg as la
from scipy.special import beta, gamma
import scipy.sparse as spar
from helper_functions import choose_pdfcdf, sigma_calc
import sgtcdfs as sgtc

def std_errs(Y, X, alphas, betas, thetas, het_form, cdf_case, deltas = None,  method='optgradient'):

    """
    Calculates the standard errors of estimated slope parameters and cutoffs. 
    Currently doesn't incorporate heteroskedasticity. 
    The ST standard errors are working (our latest edition), but I haven't checked
    them against the GQR program's standard errors, since the scaling is not the same. 


    Parameters:
    ----------------------------------------------------------------
    Y:
        categorical variable. numpy VECTOR of INTEGERS
        Assuming the categories go from 0 to 1
    X:
        Independent variables. No intercept (column of ones). Numpy array/vector
    alphas:
        Cutoff values. List or numpy vector. Cannot be just an single number!
    betas:
        Slope parameters. List or numpy vector
    deltas:
        Parameters for the sigma_form of heteroskedasticity. Currently we have only written code to calculate
        standard errors for the homoskedastic case, so this is not a working feature.
    thetas:
        Distributional parameters for whatever distribution we are using
    pdf:
        Pdf function for our specified distribution.
    cdf:
        CDF function for our specified distribution.
    sigma:
        Numpy vector for the variance of the random variable. If homoskedasticity, then 
        sigma should be a vector of ones. 

    Returns:
    ---------------------------------------------------------------
    Standard deviations for betas and alphas in a numpy vector
    """

    # ......................................Getting arguments we need......................................

    sigma = sigma_calc(het_form, len(Y), deltas)
    pdf, cdf = choose_pdfcdf(cdf_case)
    # ......................................GRADIENT MATRIX......................................
    
    k = len(alphas) + 1 #number of categories our y variable has. 
    m = len(Y)

    try:
        n = X.shape[1]
    except:
        n = 1
    xb = np.dot(X, betas)
    alpha_mat = np.zeros((m, len(alphas)))    
    beta_grad = np.zeros((m, 1))

    #creating the alpha knot column that we will use in our gradient calculation
    #and updating our first derivative beta_grad 
    a01 = Y == 0
    z0 = alphas[0] - xb
    
    
    paravec01 = [sigma[a01], thetas]
    alpha_mat[:, 0][a01] = pdf(paravec01,z0[a01])/cdf(paravec01,z0[a01])
    beta_grad[:, 0][a01] = pdf(paravec01,z0[a01])/cdf(paravec01,z0[a01])
        
    


    #creating the last alpha column that we will use in our gradient calculation
    #and updating our first derivative beta_grad 
    alast1 = Y  == len(alphas) #since our categories go from 0,...n-1, this will give us the correct category number
    
    paraveclast1 = [sigma[alast1], thetas]
    z4 = alphas[-1] - xb
    alpha_mat[:,-1][alast1] = -pdf(paraveclast1, z4[alast1])/(1-cdf(paraveclast1, z4[alast1]))
    beta_grad[:,0][alast1] = -pdf(paraveclast1, z4[alast1])/(1-cdf(paraveclast1, z4[alast1])) 

    if len(alphas) > 1:
        a02 = Y == 1
        paravec02 = [sigma[a02], thetas]
        z1 = alphas[1] - xb    
        alpha_mat[:,0][a02] = -pdf(paravec02, z0[a02])/(cdf(paravec02, z1[a02]) - cdf(paravec02, z0[a02]))
        
        z3 = alphas[-2] - xb
        alast2 = Y == len(alphas) - 1 
        paraveclast2 = [sigma[alast2], thetas]   
        alpha_mat[:,-1][alast2] = pdf(paraveclast2, z4[alast2])/(cdf(paraveclast2, z4[alast2]) - cdf(paraveclast2, z3[alast2]))

 
    #filling in the middle of our alpha columns, if we have more than two cutoff values
    if len(alphas) > 2:
        for j in xrange(1, k-2):            
            zj0 = alphas[j-1] - xb
            zj = alphas[j] - xb
            zj1 = alphas[j+1] - xb
            a1 = Y == j
            paravec1 = [sigma[a1], thetas]
            alpha_mat[:, j][a1] = pdf(paravec1, zj[a1])/(cdf(paravec1, zj[a1]) - cdf(paravec1, zj0[a1]))
            a2 = Y == j + 1
            paravec2 = [sigma[a2], thetas]
            alpha_mat[:,j][a2] = -pdf(paravec2, zj[a2])/(cdf(paravec2, zj1[a2]) - cdf(paravec2, zj[a2]))

        #completing our beta vector
        beta_bool = np.nonzero((Y!=0) & (Y!=len(alphas)))
        paravecbb = [sigma[beta_bool], thetas]


        zj = alphas[Y[beta_bool]] - xb[beta_bool]
        zj0 = alphas[Y[beta_bool]-1] - xb[beta_bool]
        beta_grad[:,0][beta_bool] = (pdf(paravecbb, zj) - pdf(paravecbb, zj0)) / (cdf(paravecbb, zj) - cdf(paravecbb, zj0))
    

    
    gradrep = np.concatenate((alpha_mat, beta_grad * -1*X), axis = 1)
    gradrep[np.isnan(gradrep)] = 0
    gradrep[np.isinf(gradrep)] = 0
    grad_exp = np.dot(gradrep.T, gradrep) #* (1/m) #expected value of the outer product of the gradient
    
    
    #...........................................HESSIAN MATRIX............................................
    #first we will get our double derivatives for the alphas.
    alpha_dd_mat = np.zeros((m, len(alphas)))
    alpha_cross_mat = np.zeros((m,len(alphas) -1))
    ab_cross_mat =  np.zeros((m, len(alphas)))
    beta_hess = np.zeros((m,1))

    #first column of alpha_dd and special case 1 for beta_hess
    paravec_gen = [sigma, thetas]
    rho1 = getrhoGQR(xb, alphas[0], paravec_gen, cdf_case)
    z0 = alphas[0] - xb

    alpha_dd_mat[:,0][a01] = -rho1[a01] * pdf(paravec01, z0[a01]) / cdf(paravec01, z0[a01]) - \
                                    (pdf(paravec01, z0[a01]) / cdf(paravec01, z0[a01])) **2

    ab_cross_mat[:,0][a01] = rho1[a01] * pdf(paravec01, z0[a01]) / cdf(paravec01, z0[a01]) + \
                            (pdf(paravec01, z0[a01]) / cdf(paravec01, z0[a01])) **2
    beta_hess[:,0][a01] = -rho1[a01] * pdf(paravec01, z0[a01]) / cdf(paravec01, z0[a01]) - \
                                    (pdf(paravec01, z0[a01]) / cdf(paravec01, z0[a01])) **2

    #last alpha_dd
    rho2 = getrhoGQR(xb, alphas[-1], paravec_gen, cdf_case)
    z4 = alphas[-1] - xb
    alpha_dd_mat[:,-1][alast1] = rho2[alast1] * pdf(paraveclast1, z4[alast1]) / (1-cdf(paraveclast1, z4[alast1])) \
                                    - (pdf(paraveclast1, z4[alast1]) / (1-cdf(paraveclast1, z4[alast1]))) ** 2
    ab_cross_mat[:,-1][alast1] = -rho2[alast1] * pdf(paraveclast1, z4[alast1]) / (1-cdf(paraveclast1, z4[alast1])) \
                                + (pdf(paraveclast1, z4[alast1]) / (1-cdf(paraveclast1, z4[alast1]))) ** 2

    beta_hess[:,0][alast1] = rho2[alast1] * pdf(paraveclast1, z4[alast1]) / (1-cdf(paraveclast1, z4[alast1]))\
                                    - (pdf(paraveclast1, z4[alast1]) / (1-cdf(paraveclast1, z4[alast1]))) ** 2



    #double derivatives and crossproducts for alphas
    if len(alphas) > 1:
        alpha_dd_mat[:,0][a02] = rho1[a02] * pdf(paravec02, z0[a02])/(cdf(paravec02, z1[a02]) - cdf(paravec02, z0[a02])) \
                                    - (pdf(paravec02, z0[a02])/(cdf(paravec02, z1[a02]) - cdf(paravec02, z0[a02]))) ** 2
        ab_cross_mat[:,0][a02] = -rho1[a02] * pdf(paravec02, z0[a02]) /(cdf(paravec02, z1[a02]) - cdf(paravec02, z0[a02])) \
                                - (pdf(paravec02, z0[a02]) * (pdf(paravec02, z1[a02]) - pdf(paravec02, z0[a02]))) / (cdf(paravec02, z1[a02]) - cdf(paravec02, z0[a02])) ** 2
        

        alpha_dd_mat[:,-1][alast2] = -rho2[alast2] * pdf(paraveclast2, z4[alast2]) / (cdf(paraveclast2, z4[alast2]) - cdf(paraveclast2, z3[alast2])) \
                                    - (pdf(paraveclast2, z4[alast2]) / (cdf(paraveclast2, z4[alast2]) - cdf(paraveclast2, z3[alast2]))) ** 2
        ab_cross_mat[:,-1][alast2] = rho2[alast2] * pdf(paraveclast2, z4[alast2]) / (cdf(paraveclast2, z4[alast2]) - cdf(paraveclast2, z3[alast2])) \
                                + pdf(paraveclast2, z4[alast2]) * (pdf(paraveclast2, z4[alast2]) - pdf(paraveclast2, z3[alast2])) / (cdf(paraveclast2, z4[alast2]) - cdf(paraveclast2, z3[alast2])) ** 2
        

        for j in xrange(0, k - 2):
            z2, z1 = alphas[j+1] - xb, alphas[j] -xb
            a = Y == j
            paravec = [sigma[a], thetas]
            alpha_cross_mat[:,j][a] = pdf(paravec, z2[a]) * pdf(paravec, z1[a]) / (cdf(paravec, z2[a]) - cdf(paravec, z1[a])) ** 2
    

    #now getting the middle alpha and beta 2nd derivatives:
    if len(alphas) > 2:

        for j in xrange(1, k -2):
            zj0, zj, zj1 = alphas[j-1] - xb, alphas[j] - xb, alphas[j+1] - xb            
            rho = getrhoGQR(xb, alphas[j], paravec_gen, cdf_case)
            a1 = Y == j            
            a2 = Y == j + 1  
            pva1 = [sigma[a1], thetas]
            pva2 = [sigma[a2], thetas]          
            alpha_dd_mat[:,j][a1] = -rho[a1] * pdf(pva1, zj[a1]) / (cdf(pva1, zj[a1]) - cdf(pva1, zj0[a1])) \
                                    - (pdf(pva1, zj[a1]) / (cdf(pva1, zj[a1]) - cdf(pva1, zj0[a1]))) ** 2
            alpha_dd_mat[:,j][a2] = rho[a2] * pdf(pva2, zj[a2]) / (cdf(pva2, zj1[a2]) - cdf(pva2, zj[a2])) \
                                    - (pdf(pva2, zj[a2]) / (cdf(pva2, zj1[a2]) - cdf(pva2, zj[a2]))) ** 2

            ab_cross_mat[:,j][a1] = rho[a1] * pdf(pva1, zj[a1]) / (cdf(pva1, zj[a1]) - cdf(pva1, zj0[a1])) \
                                    + pdf(pva1, zj[a1]) * (pdf(pva1, zj[a1]) - pdf(pva1, zj0[a1])) / (cdf(pva1, zj[a1]) - cdf(pva1, zj0[a1])) ** 2
            ab_cross_mat[:,j][a2] = -rho[a2] * pdf(pva2, zj[a2]) / (cdf(pva2, zj1[a2]) - cdf(pva2, zj[a2])) \
                                    - pdf(pva2, zj[a2]) * (pdf(pva2, zj1[a2]) - pdf(pva2, zj[a2])) / (cdf(pva2, zj1[a2]) - cdf(pva2, zj[a2])) ** 2

        beta_bool = np.nonzero((Y!=0) & (Y!=len(alphas)))
        z1 = alphas[Y[beta_bool]] - xb[beta_bool]
        z0 = alphas[Y[beta_bool]-1] - xb[beta_bool]


        rho1 = getrhoGQR(xb[beta_bool], alphas[Y[beta_bool]], paravecbb, cdf_case)
        rho0 = getrhoGQR(xb[beta_bool], alphas[Y[beta_bool] -1], paravecbb, cdf_case)
        beta_hess[:,0][beta_bool] = (-rho1 * pdf(paravecbb, z1) + rho0 * pdf(paravecbb, z0)) / (cdf(paravecbb, z1) - cdf(paravecbb, z0)) \
                                  - ((pdf(paravecbb, z1) - pdf(paravecbb, z0))/ (cdf(paravecbb, z1) - cdf(paravecbb, z0))) ** 2

    #quadrants of the hessian
    quad1 = alpha_dd_mat.sum(axis = 0)
    quad2 = np.zeros((len(alphas), n))
    quad4 = np.dot(X.T, X * beta_hess)
    for i in xrange(len(alphas)):
        ab_cross_vec = ab_cross_mat[:,i].reshape((m,1)) * X
        quad2[i,:] = ab_cross_vec.sum(axis = 0)



    if len(alphas) > 1:
        diag2 = alpha_cross_mat.sum(axis = 0)
        quad1 = spar.diags([quad1, diag2, diag2], [0, 1, -1]).todense()


    hess_exp = np.zeros((len(alphas) + n, len(alphas) + n))
    hess_exp[0: len(alphas), 0:len(alphas)] = quad1
    hess_exp[0:len(alphas), len(alphas):] = quad2
    hess_exp[len(alphas):, 0:len(alphas)] = quad2.T
    hess_exp[len(alphas):, len(alphas):] = quad4
    hess_exp[np.isnan(hess_exp)] = 0
    hess_exp[np.isinf(hess_exp)] = 0


    if method == 'optgradient':
        V = la.inv(grad_exp)
    if method == 'invneghess':
        V = la.inv(-hess_exp)
    if method == 'sandwich':
        V = np.dot(np.dot(la.inv(hess_exp), grad_exp), la.inv(hess_exp))
    
    std_errs = np.sqrt(np.diag(V))


    return std_errs


def getrhoGQR(xb, alpha, paravec, cdf_case):
    """
    One step in the chain rule when taking the derivative of the pdf 

    Parameters:
    -----------------------------------
    xb: 
        Xb vector
    alpha:
        Integer or numpy vector of alphas corresponding to the appropriate 
        observations
    paravec:
        [[sigmas], [distrubutional parameters]]. List of numpy vectors or lists. 
    cdf_case:
        1: Normal
        2: Laplace
        3: Snormal
        4: GED
        5: Slaplace
        6: SGED
        7: ST
        8: Logit
    """
    sig = paravec[0]
    thetas = paravec[1]

    if cdf_case ==1:
        rho = (alpha - xb) / (sig) ** 2
    elif cdf_case == 2:
        rho = np.sign(alpha-xb)/ sig

    elif cdf_case ==3:
        lamb = thetas[0]
        p = 2
        rho = p * abs(alpha - xb) ** (p-1) * np.sign(alpha - xb) / ((1 + lamb * np.sign(alpha - xb)) * sig) ** p
        rho[np.isnan(rho)] = 0
        rho[np.isinf(rho)] = 0
    elif cdf_case ==4:
        p = thetas[0]
        lamb = 0
        rho = p * abs(alpha - xb) ** (p-1) * np.sign(alpha - xb) / ((1 + lamb *np.sign(alpha - xb)) * sig) ** p
        rho[np.isnan(rho)] = 0
        rho[np.isinf(rho)] = 0
    elif cdf_case ==5:
        lamb = thetas[0]
        p = 1
        rho = p * abs(alpha - xb) ** (p-1) * np.sign(alpha - xb) / ((1 + lamb * np.sign(alpha - xb)) * sig) ** p
        rho[np.isnan(rho)] = 0
        rho[np.isinf(rho)] = 0
    elif cdf_case ==6:
        lamb, p = thetas
        rho = p * abs(alpha - xb) ** (p-1) * np.sign(alpha - xb) / ((1 + lamb * np.sign(alpha - xb)) * sig) ** p
        rho[np.isnan(rho)] = 0
        rho[np.isinf(rho)] = 0
    elif cdf_case == 7:
        lamb, q = thetas
        p = 2
        rho = (p*q + 1) * np.sign(alpha-xb)*abs(alpha-xb)**(p-1) / (q * sig**p * \
            (1+lamb*np.sign(alpha-xb))**p + abs(alpha-xb)**p)
        rho[np.isnan(rho)] = 0
        rho[np.isinf(rho)] = 0
    elif cdf_case == 8:
        rho = (np.e ** -xb - 1) / (np.e ** -xb + 1)

    return rho



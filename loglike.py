# Written by Carla
# last updated August 6, 2018 by Carla

from __future__ import division
from helper_functions import sigma_calc, choose_pdfcdf
import numpy as np

def lr_test(Y, X, method_opt, options, est_param):
    """
    Calculates the loglikehood ratio test for a specified model
    :param Y:
        numpy vector containing the ordinal Y values(must be integers and start at 0)
    :param X:
        numpy array containing the x-variable observations
    :param method_opt:
        String. Optimization method to use for the scipy.optimize minimize function. Acceptable methods can be found here:
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html
    :param options:
        Dictionary of scipy.optimize.minimize options such as maximum number of iterations and tolerance levels
    :param est_param:
        The resultant object from from optimize_func() in optimize.py
    :return:
        Scalar. Result of the loglikelihood test
    """
    ll_restricted = restricted_calc(Y, X, method_opt, options)
    ll_unrestricted = -1 * est_param.fun
    lr_test = 2 * (ll_unrestricted - ll_restricted)
    return lr_test


def llv(paravec_ab, thetas, X, Y, cdf_case, k, het_form, deltas=None):
    """
    Calculates the probability of a set of ordered outcomes assuming the 
    given cumulative distribution function. 

    Parameters:
    .................................
    paravec_ab:
        Includes all parameters needed to be estimated. Can be described as the
        following:
        a_0, a_1, ... a_(j-1), b_0, b1, ... b_(k-1) where a_j are the cutoff values and 
        b_i are the slope parameters.
        An intercept/constant is not included. 
    thetas: 
        distributional parameters. 
    X: 
        numpy array containing the x-variable observations
    Y:  
        numpy vector containing the ordinal Y values(must be integers and start at 0)
    cdf:
        Python function: cdf(paravec, data)
        Specified cdf we assume for our data. (Eg. normal, logistic, skewed t...)    
    k:
        Number of categories the Y variable spans
    sign:
        If we would like to minimize, then we must multiply by -1

    Returns:
    ------------------------------------------
    LLV_sum:
        The probability of the random variable Y occurring 
    """
    pdf, cdf = choose_pdfcdf(cdf_case)
    m = len(Y)


    # putting things in order
    alphas = paravec_ab[0:k-1]
    betas = paravec_ab[k-1:]
    sigmas = sigma_calc(het_form, m, deltas)
    LLV = np.zeros(m)

    alphas_two = np.concatenate((alphas,[0.0]))
    alphas_one = np.concatenate(([0.0], alphas))
    endpoints_two=alphas_two[Y]
    endpoints_one=alphas_one[Y]

    xb = np.dot(X, betas)  

    # Calculating the log-likelihood for the first cutoff:
    first_cutoff = Y == 0
    cdf_param = [sigmas[first_cutoff], thetas]
    z = alphas[0] - xb[first_cutoff]
    prob = cdf(cdf_param, z)
    LLV[first_cutoff] = np.log(prob)

    # last cutoff
    last_cutoff = Y == k-1
    cdf_param = [sigmas[last_cutoff], thetas]
    z = alphas[k-2] - xb[last_cutoff]
    prob = 1 - cdf(cdf_param, z)
    LLV[last_cutoff] = np.log(prob)

    z2 = np.zeros(m)
    z1 = np.zeros(m)

    middle_cutoff= ~(last_cutoff|first_cutoff)
    cdf_param = [sigmas[middle_cutoff], thetas]
    z2[middle_cutoff] = endpoints_two[middle_cutoff] - xb[middle_cutoff]
    z1[middle_cutoff] = endpoints_one[middle_cutoff] - xb[middle_cutoff]

    LLV[middle_cutoff] = np.log(cdf(cdf_param, z2[middle_cutoff])-cdf(cdf_param, z1[middle_cutoff]))
    LLV_sum = LLV.sum()
    return -LLV_sum


def restricted_calc(Y, X, method = 'Nelder-Mead', options=({'maxiter': 20000, 'maxfev' : 20000})):
    """
    Calculating the restricted log-likelihood. Basically, we estimate where the cutoffs should be on
    the categorical variable and then compute the loglikelihood of that model (no x's, just bins for y)

    Returns the loglikelihood of the model sans x's.
    """
    # k = len(set(Y))
    # n = X.shape[1]
    # start_betas = np.zeros(n)
    # ols_betas = la.lstsq(X, Y)[0]
    # alphas = []

    # #getting alpha estimates using predicted y's from OLS.
    # y_pred = np.dot(X, ols_betas)
    # max_min = y_pred.max() - y_pred.min()
    # for i in xrange(1, k):
    #     alphas.append(max_min * (i/k) + y_pred.min())
    # start_alphas = np.asarray(alphas)

    # loglike_obj = lambda x: llv(np.concatenate((x, start_betas)), [0], X, Y, sgtc.normal_cdf, k, None)
    # est_param = minimize(loglike_obj, start_alphas, method = method, options = options)

    # return -est_param.fun

    Y_values=set(Y)
    Y_total=len(Y)
    list_Y=list(Y)
    llv=0
    for val in Y_values:
        val_count=list_Y.count(val)
        llv+=val_count*np.log(val_count/Y_total)
    return llv



